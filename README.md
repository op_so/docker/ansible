# Docker alpine `Ansible`

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/docker/ansible?branch=master&style=for-the-badge)](https://gitlab.com/op_so/docker/ansible/pipelines)

A Docker image to run [`Ansible`](https://docs.ansible.com/ansible/latest/index.html) playbooks. [`Ansible-lint`](https://docs.ansible.com/ansible-lint/) to test playbooks is also included.

* **lightweight** image based on Alpine Linux 150 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-554488?logo=gitlab&style=for-the-badge)](https://gitlab.com/op_so/docker/ansible) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-1D63ED?logo=docker&logoColor=white&style=for-the-badge)](https://hub.docker.com/r/jfxs/ansible) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-E5141F?logo=redhat&logoColor=white&style=for-the-badge)](https://quay.io/repository/ifxs/ansible) The Quay.io registry.

Since `Ansible` version 2.10, the Docker image has only ansible-base without any collection. It could be necessary to install them.
This image includes:

* [`ansible-core`](https://docs.ansible.com/ansible/latest/index.html)
* [`ansible-lint`](https://ansible-lint.readthedocs.io/)
* [`go-task`](https://taskfile.dev/)
* [`hvac`](https://github.com/hvac/hvac)
* [`jc`](https://github.com/kellyjonbrazil/jc)
* [`testinfra`](https://testinfra.readthedocs.io/en/latest/)
* [`yamllint`](https://yamllint.readthedocs.io/en/stable/)
* [`yq`](https://github.com/mikefarah/yq)

## Running `Ansible`

Example to run `Ansible` playbooks in your current directory:

```shell
docker run -it --rm -v $(pwd):/ansible jfxs/ansible ansible-playbook playbook.yml
```

To install collections:

```shell
docker run -it --rm -v $(pwd):/ansible jfxs/ansible /bin/sh -c "ansible-galaxy collection install ansible.utils && ansible-playbook playbook.yml"
```

To test playbooks in your current directory with `ansible-lint`:

```shell
docker run -it --rm -v $(pwd):/ansible jfxs/ansible ansible-lint playbook.yml
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/ansible/-/blob/master/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/ansible) has the details of the last published image.

## Versioning

Docker tag definition:

* the `Ansible` version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<ansible_version>-<increment>
```

Example: 2.14.2-001

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the `SBOM` attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
