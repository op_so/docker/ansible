# hadolint ignore=DL3007
FROM python:3-alpine

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container=docker
ENV HOME=/.ansible

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="Ansible" \
    org.opencontainers.image.description="A lightweight docker image to run Ansible playbooks" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/ansible" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/ansible" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL3018
RUN apk --no-cache add \
       ca-certificates \
       curl \
       git \
       jq \
       openssh-client \
       openssl \
       py3-cryptography \
       py3-packaging \
       py3-pip \
       py3-six \
# For libyaml = True
       py3-yaml \
       rsync \
       sshpass \
       go-task \
       vim \
       yq

COPY files/ssh_config /etc/ssh/ssh_config
COPY files/ansible.cfg /etc/ansible/ansible.cfg

# hadolint ignore=DL3018,DL3013,DL4006
RUN apk --no-cache add --virtual .build-deps \
       build-base \
       libffi-dev \
       openssl-dev \
       python3-dev \
    && pip3 install --no-cache-dir --upgrade \
       cffi \
       ansible-core==${VERSION} \
       ansible-lint \
       hvac \
       jc \
       jmespath \
       netaddr \
# Avoid warning on ansible-lint
       pytest \
       pytest-testinfra[ansible] \
       yamllint \
    && apk --no-cache del .build-deps \
    && rm -rf /ansible/.cache \
# hadolint ignore=DL3059
    && ln -s /usr/bin/go-task /usr/bin/task \
    && addgroup nobody root \
    && mkdir -p /ansible /.ansible/cp /.ansible/tmp \
    && chgrp -R 0 /ansible /.ansible /.ansible/cp /.ansible/tmp \
    && chmod -R g=u /etc/passwd /ansible /.ansible /.ansible/cp /.ansible/tmp

WORKDIR /ansible

COPY files/uid-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["uid-entrypoint.sh"]

USER 10010
CMD ["ansible", "--version"]
