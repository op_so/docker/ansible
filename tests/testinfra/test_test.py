# -*-coding:utf-8 -*

def test_file_is_created(host):
    test_file = host.file("/tmp/test-file.txt")
    assert test_file.exists
    assert test_file.user == "root"
    assert test_file.group == "root"
    assert test_file.mode == 0o644
