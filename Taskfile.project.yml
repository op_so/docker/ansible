---
# https://taskfile.dev
#
# Taskfile.project.yml for your main project tasks. Must be commited.
# If you always want the last version of the task templates, add the following line in your .gitignore file
# /Taskfile.d/
#
version: '3'

vars:
  # TO MODIFY: Task templates to download separated by comma
  # Example: TASK_TEMPLATES: go,lint,yarn
  TASK_TEMPLATES: ansible,docker,git,lint,version
  DEFAULT_TAG: ansible

tasks:

  00-get-list-templates:
    # Get the list of templates to download
    # Do not remove
    cmds:
      - echo "{{.TASK_TEMPLATES}}"
    silent: true

  10-build-local:
    desc: "[PROJECT] Build an image locally. Arguments: [TAG|T=ansible:latest] [VCS_REF|C=110f273aad1cc] [FILE|F=<Dockerfile_path>] (*)"
    summary: |
      [PROJECT] Build an image locally.
      Usage: task 00:10-build-local [TAG|T=<image[:tag]>] [VCS_REF|C=<commit_sha>] [FILE|F=<Dockerfile_path>]

      Arguments:
       TAG     | T  Tag of the image (optional, default {{.FILE_TASK_START}})
       VCS_REF | C  Commit revision SHA hash (optional, by default NO_REF)
       FILE    | F  Dockerfile path (optional, by default Dockerfile)
    vars:
      TAG: '{{.TAG | default .T | default .DEFAULT_TAG}}'
      ANSIBLE_VERSION:
        sh: task version:get-latest-pypi PACKAGE=ansible-core
      VCS_REF: '{{.VCS_REF | default .C | default "NO_REF"}}'
      FILE: '{{.FILE | default .F | default "Dockerfile"}}'
    cmds:
      # - task docker:build-local TAG="{{.TAG}}" VERSION="{{.ANSIBLE_VERSION}}" VCS_REF="{{.VCS_REF}}" FILE="{{.FILE}}"
      - task docker:build-local TAG="{{.TAG}}" VERSION=2.18.0 VCS_REF="{{.VCS_REF}}" FILE="{{.FILE}}"
    silent: false

  20-test-local:
    desc: "[PROJECT] Test an image. Arguments: [IMG|I=alpine:latest] (*)"
    summary: |
      [PROJECT] Test an image.
      Usage: task 00:20-sanity-test1 [IMG|I=<docker_image:tag>]

      Arguments:
       IMG | I  Docker image to test (optional, default {{.FILE_TASK_START}})
    vars:
      IMG: '{{.IMG | default .I | default .DEFAULT_TAG}}'
    cmds:
      - docker run -t --rm {{.IMG}} /bin/sh -c "ansible --version"
      - docker run -t --rm {{.IMG}} /bin/sh -c "ansible-lint --version"
      - docker run -t --rm {{.IMG}} /bin/sh -c "py.test --version"
      - docker run -t --rm -v $(pwd)/tests:/ansible {{.IMG}} /bin/sh -c "export ANSIBLE_CONFIG=/ansible/ansible.cfg && ansible-galaxy install -r requirements.yml && ansible-lint --offline --project-dir . --exclude .cache --exclude .ansible playbook.yml"
      - docker run -t --rm -v $(pwd)/tests:/ansible {{.IMG}} /bin/sh -c "export ANSIBLE_CONFIG=/ansible/ansible.cfg && ansible-galaxy install -r requirements.yml && ansible-playbook --limit local --vault-password-file vault-password-file.dist -i inventory playbook.yml"
    silent: true

  30-test-remote:
    desc: "[PROJECT] Test an image in remote SSH. Arguments: [IMG|I=alpine:latest] (*)"
    summary: |
      [PROJECT] Test an image.
      Usage: task 00:30-sanity-test2 [IMG|I=<docker_image:tag>]

      Arguments:
       IMG | I  Docker image to test (optional)
    vars:
      IMG: '{{.IMG | default .I | default .DEFAULT_TAG}}'
      ANSIBLE_NETWORK: ansible-test-network
      IMAGE_SSHD: takeyamajp/ubuntu-sshd:ubuntu22.04
      HOST: ssh-host
      PASS: pass-4-Test
      TZ: Etc/UTC
    cmds:
      - docker network create {{.ANSIBLE_NETWORK}}
      - defer: docker network rm {{.ANSIBLE_NETWORK}}
      - docker run -d --rm --name {{.HOST}} --network {{.ANSIBLE_NETWORK}} -e ROOT_PASSWORD={{.PASS}} -e TZ={{.TZ}} {{.IMAGE_SSHD}}
      - defer: docker stop {{.HOST}}
      - docker run -t --rm --network {{.ANSIBLE_NETWORK}} -v $(pwd)/tests:/ansible {{.IMG}} /bin/sh -c "ansible-galaxy install -r requirements.yml && ansible-playbook --limit remote playbook.yml"
      - docker run -t --rm --network {{.ANSIBLE_NETWORK}} -v $(pwd)/tests:/ansible {{.IMG}} /bin/sh -c "ansible-playbook --limit remote playbook.yml | grep -q 'changed=0.*failed=0' && (printf '\033[0;32m[OK] Idempotence test passed.\033[0m\n\n' && exit 0) || (printf '\033[0;31m[KO] Idempotence test failed!\033[0m\n\n' && exit 1)"
      - docker run -t --rm --network {{.ANSIBLE_NETWORK}} -v $(pwd)/tests:/ansible {{.IMG}} /bin/sh -c "py.test -v --hosts='ansible://remote' --ansible-inventory=inventory testinfra/test_test.py"
    silent: true

  40-pre-commit:
    desc: "[PROJECT] Pre-commit checks."
    cmds:
      - date > {{.FILE_TASK_START}}
      - defer: rm -f {{.FILE_TASK_START}}
      - task lint:pre-commit
      - task lint:all MEX='"#Taskfile.d" "#.cache"'
      - echo "Checks Start $(cat {{.FILE_TASK_START}}) - End $(date)"
    silent: true
